#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from inspect import signature
from io import StringIO

from pytest import raises

from ycontract._out_contract import (
    OutContractException,
    call_condf,
    new_out_contract_exception,
    out_contract,
)
from ycontract.ycontract import ContractError, ContractState


def add(a: int, b: int, c: int, d: int = 2, e: int = 3):
    return a + b + c + d + e


def test_new_out_contract_exception():

    def add(a, b, c, d=3, e=10):
        return a + b + c

    def is_positive(a: int) -> bool:
        return a > 0

    ex = new_out_contract_exception(add, [1], {"b": 3, "c": 5}, is_positive, 22, contract_tag=None)
    assert isinstance(ex, OutContractException)
    ex_msg = ex.args[0]
    ex_pattern = "^\n"
    ex_pattern += r"    function:  add at \S+test_out_contract.py:\d+" + "\n"
    ex_pattern += r"    condition: is_positive at \S+test_out_contract.py:\d+" + "\n"
    ex_pattern += r"    arguments: a=1, b=3, c=5, d=3, e=10" + "\n"
    ex_pattern += r"    result:    22"
    ex_pattern += r"$"
    assert re.search(ex_pattern, ex_msg), ex_msg


def test_new_out_contract_exception_for_contract_tag():

    def add(a, b, c, d=3, e=10):
        return a + b + c

    def is_positive(a: int) -> bool:
        return a > 0

    ex = new_out_contract_exception(
        add, [1], {
            "b": 3,
            "c": 5
        }, is_positive, 22, contract_tag="tagtag")
    assert isinstance(ex, OutContractException)
    ex_msg = ex.args[0]
    ex_pattern = "^\n"
    ex_pattern += r"    tag:       tagtag" + "\n"
    ex_pattern += r"    function:  add at \S+test_out_contract.py:\d+" + "\n"
    ex_pattern += r"    condition: is_positive at \S+test_out_contract.py:\d+" + "\n"
    ex_pattern += r"    arguments: a=1, b=3, c=5, d=3, e=10" + "\n"
    ex_pattern += r"    result:    22"
    ex_pattern += r"$"
    assert re.search(ex_pattern, ex_msg), ex_msg


def test_call_condf_fullmatch_with_one_arg():

    def f(x):
        return x * x

    def cond_f(res, _):
        return res > 0

    assert call_condf(cond_f, f, f(1), [1], {})
    assert not call_condf(cond_f, f, f(0), [0], {})


def test_call_condf_fullmatch_with_some_arg():

    def f(x, y):
        return x * x + y * y

    def cond_f(res, a, b):
        return a > 0

    assert call_condf(cond_f, f, f(1, 2), [1, 2], {})
    assert not call_condf(cond_f, f, f(-1, 2), [-1, 2], {})


def test_call_condf_fullmatch_with_some_kw():

    def f(x, y=2):
        return x * x + y * y

    def cond_f(res, a, b):
        return a > 0

    assert call_condf(cond_f, f, f(1, 2), [1], {"y": 2})
    assert not call_condf(cond_f, f, f(-1, 2), [-1], {"y": 2})


def test_call_condf_partial_args():

    def f(x, y, z=100, w=1000):
        return x * x + y * y

    def cond_f(_, x):
        return x > 0

    assert call_condf(cond_f, f, f(1, 2), [1], {"y": 2})
    assert not call_condf(cond_f, f, f(-1, 2), [-1], {"y": 2})


def test_out_contract_fix_signature():

    def add(a, b, c, d=2, e=3):
        return a + b + c + d + e

    add2 = out_contract()(add)
    assert signature(add) == signature(add2)


def test_out_contract_fix_signature_case_disable(disable_contract_state: ContractState):

    def add(a, b, c, d=2, e=3):
        return a + b + c + d + e

    add2 = out_contract(contract_state=disable_contract_state)(add)
    assert signature(add) == signature(add2)


def test_out_contract_only_once_output():

    io = StringIO()

    @out_contract()
    def show(s: str):
        print(s, file=io)

    show("Hello")
    text = io.getvalue()
    assert text.count("Hello") == 1


def test_out_contract():

    @out_contract(lambda res: res > 0)
    def sub(a, b):
        return a - b

    with raises(OutContractException):
        sub(1, 2)

    sub(2, 1)


def test_out_contract_some_conditions():

    @out_contract(
        lambda res: res > 0,
        lambda res: res < 4,
    )
    def sub(a, b):
        return a - b

    with raises(OutContractException):
        sub(1, 2)

    with raises(OutContractException):
        sub(5, 1)

    sub(2, 1)


def test_out_contract_for_contract_tag():

    @out_contract(lambda res: res > 0, contract_tag="tagtag")
    def sub_for_contract_tag(a, b):
        return a - b

    with raises(OutContractException, match="tagtag"):
        sub_for_contract_tag(1, 2)


def test_out_contract_for_disable_contract_state(disable_contract_state: ContractState):

    @out_contract(lambda res: res > 0, contract_state=disable_contract_state)
    def sub_for_disable_test(a, b):
        return a - b

    sub_for_disable_test(1, 2)


def test_out_contract_only_once_output_for_disable_contract_state(
        disable_contract_state: ContractState):

    io = StringIO()

    @out_contract(contract_state=disable_contract_state)
    def show(s: str):
        print(s, file=io)

    show("Hello")
    text = io.getvalue()
    assert text.count("Hello") == 1


def test_out_contract_for_disable2():
    contract_state = ContractState()

    @out_contract(lambda a: a > 0, contract_state=contract_state)
    def f(a):
        return a

    contract_state.disable()

    f(-1)


def test_out_contract_for_contract_error():

    @out_contract(lambda _, a, b, c: a > 0)
    def sub_for_disable_test(a, b):
        return a - b

    with raises(ContractError):
        sub_for_disable_test(1, 2)
