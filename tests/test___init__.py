#!/usr/bin/env python
# -*- coding: utf-8 -*-

# flake8: noqa

from ycontract import *


def test_import():

    imported = set()
    imported |= {'contract'}  # from _contract
    imported |= {'InContractException', 'in_contract'}  # from _in_contract
    imported |= {'OutContractException', 'out_contract'}  # from _out_contract
    imported |= {
        'DEFAULT_CONTRACT_STATE', 'ContractError', 'ContractException', 'ContractState',
        'disable_contract'
    }  # from ycontract
    imported |= {
        'more_than', 'less_than', 'more_than_or_eq', 'less_than_or_eq', 'in_range', 'is_sub',
        'is_type', 'is_instance', 'is_in', 'exists_path'
    }  # from util

    expected = {
        s for s in globals()
        if not s.startswith("_") and not s.startswith("@") and not s.startswith("test_")
    }

    assert imported == expected
