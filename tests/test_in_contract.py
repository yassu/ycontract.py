#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from inspect import signature
from io import StringIO
from typing import Dict, List

from frozendict import frozendict
from pytest import raises

from ycontract._in_contract import (
    InContractException,
    get_contract_exception_from_dict,
    in_contract,
    new_in_contract_exception,
)
from ycontract.ycontract import ContractError, ContractState


def test_new_in_contract_exception():

    def add(a, b, c, d=3, e=10):
        return a + b + c

    def is_positive(a: int) -> bool:
        return a > 0

    ex = new_in_contract_exception(add, [-1], {"b": 3, "c": 5}, is_positive, contract_tag=None)
    assert isinstance(ex, InContractException)
    ex_msg = ex.args[0]
    ex_pattern = "^\n"
    ex_pattern += r"    function:  add at \S+test_in_contract.py:\d+" + "\n"
    ex_pattern += r"    condition: is_positive at \S+test_in_contract.py:\d+" + "\n"
    ex_pattern += r"    arguments: a=-1, b=3, c=5, d=3, e=10"
    ex_pattern += r"$"
    assert re.search(ex_pattern, ex_msg), ex_msg


def test_new_in_contract_exception_for_contract_tag():

    def add(a, b, c, d=3, e=10):
        return a + b + c

    def is_positive(a: int) -> bool:
        return a > 0

    ex = new_in_contract_exception(add, [-1], {"b": 3, "c": 5}, is_positive, contract_tag="tagtag")
    assert isinstance(ex, InContractException)
    ex_msg = ex.args[0]
    ex_pattern = "^\n"
    ex_pattern += r"    tag:       tagtag" + "\n"
    ex_pattern += r"    function:  add at \S+test_in_contract.py:\d+" + "\n"
    ex_pattern += r"    condition: is_positive at \S+test_in_contract.py:\d+" + "\n"
    ex_pattern += r"    arguments: a=-1, b=3, c=5, d=3, e=10"
    ex_pattern += r"$"
    assert re.search(ex_pattern, ex_msg), ex_msg


def test_in_contract_callable_full_match_case():

    @in_contract(lambda a, b, c, d, e: a > 0)
    def add_(a2, b2, c2, d2=2, e2=3):
        return a2 + b2 + c2 + d2 + e2

    with raises(InContractException, match="<lambda>"):
        add_(-1, 0, 0, 0, 0)

    add_(3, 2, 1)


def test_in_contract_callable_partial_match_case():

    @in_contract(lambda a: a > 0)
    def add_(a, b, c, d=2, e=3):
        return a + b + c + d + e

    with raises(InContractException):
        assert add_(-1, 0, 0, 0, 0)

    add_(3, 2, 1)


def test_in_contract_callable_partial_match_case_with_several_variables():

    @in_contract(lambda a, b: a * b > 0)
    def add(a, b, c, d=2, e=3):
        return a + b + c + d + e

    with raises(InContractException):
        assert add(-1, 2, 0, 0, 0)

    add(1, 2, 1)


def test_in_contract_dict_match_varname_case_for_one_variable():

    @in_contract({"a": lambda x: x > 0})
    def add(a, b, c, d=2, e=3):
        return a + b + c + d + e

    with raises(InContractException):
        assert add(-1, 2, 0, 0, 0)

    add(1, 2, 1)


def test_in_contract_dict_match_varname_case_for_several_variable():

    @in_contract({("a", "b"): lambda x, y: x * y > 0})
    def add_(a, b, c, d=2, e=3):
        return a + b + c + d + e

    with raises(InContractException):
        assert add_(-1, 2, 0, 0, 0)

    add_(1, 2, 1)


def test_get_contract_exception_from_dict_with_one_variable(enable_contract_state: ContractState):

    def add_(a, b, c, d=2, e=3):
        return a + b + c + d + e

    assert get_contract_exception_from_dict(
        lambda x: x >= 0, add_, "a", None, enable_contract_state, *[1, 1, 1]) is None
    assert isinstance(
        get_contract_exception_from_dict(
            lambda x: x >= 0, add_, "a", None, enable_contract_state, *[-1, -1, -1]),
        InContractException)


def test_get_contract_exception_from_dict_with_some_variable(enable_contract_state: ContractState):

    def add_(a, b, c, d=2, e=3):
        return a + b + c + d + e

    assert get_contract_exception_from_dict(
        lambda x, y: x >= 0 and y >= 0, add_,
        ("a", "b"), None, enable_contract_state, *[1, 1, 1]) is None
    assert isinstance(
        get_contract_exception_from_dict(
            lambda x, y: x >= 0 and y >= 0, add_, ("a", "b"), None, enable_contract_state,
            *[-1, -1, -1]), InContractException)

    assert get_contract_exception_from_dict(
        lambda x, y: x >= 0 and y >= 0, add_,
        ("a", "b"), None, enable_contract_state, *[1, 1, 1]) is None
    assert isinstance(
        get_contract_exception_from_dict(
            lambda x, y: x >= 0 and y >= 0, add_, ("a", "b"), None, enable_contract_state,
            *[-1, -1, -1]), InContractException)


def test_in_contract_fix_signature():

    def add(a, b, c, d=2, e=3):
        return a + b + c + d + e

    add2 = in_contract()(add)
    assert signature(add) == signature(add2)


def test_in_contract_fix_signature_case_disable(disable_contract_state: ContractState):

    def add(a, b, c, d=2, e=3):
        return a + b + c + d + e

    add2 = in_contract(contract_state=disable_contract_state)(add)
    assert signature(add) == signature(add2)


def test_in_contract_only_once_output():

    io = StringIO()

    @in_contract()
    def show(s: str):
        print(s, file=io)

    show("Hello")
    text = io.getvalue()
    assert text.count("Hello") == 1


def test_in_contract_for_contract_tag():

    @in_contract(lambda: False, contract_tag="add7")
    def add_(a: int, b: int, c: int, d: int = 2, e: int = 3):
        return a + b + c + d + e

    with raises(InContractException, match="add7"):
        add_(-1, 2, 0, 0, 0)


def test_in_contract_for_disable(disable_contract_state: ContractState):

    @in_contract(lambda a: False, contract_state=disable_contract_state)
    def add_(a: int, b: int, c: int, d: int = 2, e: int = 3):
        return a + b + c + d + e

    assert add_(-1, 2, 0, 0, 0)


def test_in_contract_for_disable2():

    contract_state = ContractState()

    @in_contract(lambda a: a > 0, contract_state=contract_state)
    def f(a):
        return a

    contract_state.disable()

    f(-1)


def test_in_contract_for_cond_kw():

    @in_contract(a=lambda x: x > 0)
    def add(a, b, c, d=2, e=3):
        return a + b + c + d + e

    with raises(InContractException):
        assert add(-1, 0, 0, 0, 0)

    add(1, 0, 0, 0, 0)


def test_in_contract_for_contract_error():

    @in_contract(lambda a, b: a * b > 0)
    def add_for_contract_error(a, d, c):
        return a + d

    with raises(ContractError):
        add_for_contract_error(1, 2, 3)


def test_in_contract_for_complex_case():

    @in_contract(
        lambda a0: a0 > 0,
        lambda a1, b1: a1 > 0 and b1 > 0,
        {"a2": lambda x: x > 0},
        {("a3",): lambda x: x > 0},
        {("a4", "a5"): lambda x, y: x > 0 and y > 0},
        b0=lambda b: b > 0,
        b1=lambda b: b > 0,
        b2=lambda b: b > 0,
    )
    def add_(a0, a1, a2, a3, a4, a5, b0=1, b1=1, b2=1):
        return a0 + a1 + a2 + a3 + a4 + a5 + (b0 + b1)

    default_args = (
        1,
        1,
        1,
        1,
        1,
        1,
    )
    default_opts = frozendict({
        "b0": 1,
        "b1": 1,
        "b2": 1,
    })

    def updated_args(kw: Dict[int, int]) -> List[int]:
        args = list(default_args)
        for key, value in kw.items():
            args[key] = value
        return args

    def updated_opts(kw: Dict[str, int]) -> Dict[str, int]:
        opts = dict(default_opts)
        for key, value in kw.items():
            opts[key] = value
        return opts

    add_(*default_args)
    add_(*default_args, **default_opts)

    for i in range(len(default_args)):
        args = updated_args({0: -1})
        with raises(InContractException):
            add_(*args)
        with raises(InContractException):
            add_(*args, **default_opts)

    for key in default_opts.keys():
        opts = updated_opts({key: -1})
        with raises(InContractException):
            add_(*default_args, **opts)


def test_in_contract_same_var_by_kw():

    @in_contract(a=[lambda x: x > 0, lambda x: x < 4])
    def add(a, b):
        return a + b

    with raises(InContractException):
        add(0, 2)

    with raises(InContractException):
        add(4, 2)

    add(1, 2)


def test_in_contract_same_var_by_dict():

    @in_contract({"a": [lambda x: x > 0, lambda x: x < 4]})
    def add(a, b):
        return a + b

    with raises(InContractException):
        add(0, 2)

    with raises(InContractException):
        add(4, 2)

    add(1, 2)
