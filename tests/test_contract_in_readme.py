#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import Dict, List

from pytest import raises

from ycontract import InContractException, OutContractException, contract


@contract(lambda a, b: a * b > 0)
def add(a, b, c):
    return a + b


def test_add_valid():
    add(1, 2, 0)
    add(-1, -2, 0)


def test_add_invalid():
    with raises(InContractException):
        add(1, -2, 0)


@contract(returns=lambda res: res > 0)
def sub(a, b):
    return a - b


def test_sub_invalid():
    with raises(OutContractException):
        sub(1, 2)


@contract(
    lambda a0: a0 > 0,
    lambda a1, b: a1 > 0 and b > 0,
    {
        "a2": lambda x: x > 0,
        "a3": [
            lambda x: x >= 0,
            lambda x: x < 4,
        ],
        ("a4", "a5"): lambda x, y: x > 0 and y > 0,
    },
    b=lambda x: x > 0,
    contract_tag="tagtag",
)
def add_for_complex(a0, a1, a2, a3, a4, a5, b=1):
    return a0 + a1 + a2 + a3 + a4 + a5 + b


def test_add_for_complex():
    default_args = (
        1,
        1,
        1,
        0,
        1,
        1,
    )
    add_for_complex(*default_args)

    def updated_args(kw: Dict[int, int]) -> List[int]:
        args = list(default_args)
        for key, value in kw.items():
            args[key] = value
        return args

    for i in range(len(default_args)):
        args = updated_args({0: -1})
        with raises(InContractException):
            add_for_complex(*args)

    add_for_complex(*default_args, b=2)
    with raises(InContractException):
        add_for_complex(*default_args, b=-1)


def test_add_for_second_contract():
    with raises(InContractException):
        add_for_complex(1, 1, 1, -1, 1, 1)

    add_for_complex(1, 1, 1, 3, 1, 1)

    with raises(InContractException):
        add_for_complex(1, 1, 1, 4, 1, 1)


def test_add_for_complex_for_tag():
    with raises(InContractException, match="tagtag"):
        add_for_complex(-1, 1, 1, 1, 1, 1)
