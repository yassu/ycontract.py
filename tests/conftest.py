#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from ycontract import ContractState


@pytest.fixture(scope='module')
def enable_contract_state():
    return ContractState(True)


@pytest.fixture(scope='module')
def disable_contract_state():
    return ContractState(False)
