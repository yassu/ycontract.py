#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections.abc import Collection
from dataclasses import dataclass
from pathlib import Path
from typing import Tuple

import pytest
from pytest import raises

from ycontract import ContractError
from ycontract.conds import _to_native_number as to_native_number
from ycontract.conds import (
    exists_path,
    in_range,
    is_in,
    is_instance,
    is_negative,
    is_positive,
    is_sub,
    is_type,
    less_than,
    less_than_or_eq,
    more_than,
    more_than_or_eq,
    not_f,
)


@dataclass
class TestInt:
    n: int

    def __int__(self):
        return self.n


@dataclass
class TestFloat:
    f: float

    def __float__(self):
        return self.f


@dataclass
class TestCollection:
    c: Collection

    def __contains__(self, x):
        return x in self.c

    def __len__(self):
        return len(self.c)

    def __iter__(self):
        return iter(self.c)


to_native_number_params = [
    (TestFloat(-1.0), -1.0),
    (TestFloat(-1.0e-4), -1.0e-4),
    (TestFloat(0.0), 0.0),
    (TestFloat(-0.0), -0.0),
    (TestFloat(1.0e-4), 1.0e-4),
    (TestFloat(1.0), 1.0),
    (TestInt(-1), -1),
    (TestInt(0), 0),
    (TestInt(1), 1),
]


@pytest.mark.parametrize("x, expect", to_native_number_params)
def test_to_native_number(x, expect):
    assert to_native_number(x) == expect


def test_to_native_number_raise_contract_error():
    with raises(ContractError):
        assert to_native_number("abc")


def test_CondFunction_or():
    cond_f = more_than_or_eq(1) | less_than_or_eq(-1)
    expects = (
        (-2, True),
        (-1, True),
        (0, False),
        (1, True),
        (2, True),
    )
    for target, expect in expects:
        assert cond_f(target) is expect, f"target={target}, expect={expect}"


def test_CondFunction_xor():
    cond_f = more_than_or_eq(0) ^ less_than_or_eq(0)
    expects = (
        (-2, True),
        (-1, True),
        (0, False),
        (1, True),
        (2, True),
    )
    for target, expect in expects:
        assert cond_f(target) is expect, f"target={target}, expect={expect}"


def test_CondFunction_and():
    cond_f = more_than_or_eq(0) & less_than_or_eq(0)
    expects = (
        (-2, False),
        (-1, False),
        (0, True),
        (1, False),
        (2, False),
    )
    for target, expect in expects:
        assert cond_f(target) is expect, f"target={target}, expect={expect}"


not_f_params = (
    (lambda _: True, False),
    (lambda _: False, True),
)


@pytest.mark.parametrize("cond_f, expect", not_f_params)
def test_not_f(cond_f, expect):
    not_func = not_f(cond_f)
    assert not_func(1) is expect


more_than_params = [
    (1.0, 1.0 + 1.0e-4, True),
    (1.0, 1.0, False),
    (1.0, 1.0 - 1.0e-4, False),
    (-1.0, -1.0 + 1.0e-4, True),
    (-1.0, -1.0, False),
    (-1.0, -1.0 - 1.0e-4, False),
]


@pytest.mark.parametrize("more_than_target, x, expect", more_than_params)
def test_more_than(more_than_target, x, expect):
    assert more_than(more_than_target)(x) is expect


is_positive_params = [
    (-1e-4, False),
    (0, False),
    (1e-4, True),
]


@pytest.mark.parametrize("n, expect", is_positive_params)
def test_is_positive(n, expect):
    assert is_positive(n) is expect


less_than_params = [
    (1.0, 1.0 - 1.0e-4, True),
    (1.0, 1.0, False),
    (1.0, 1.0 + 1.0e-4, False),
    (-1.0, -1.0 + 1.0e-4, False),
    (-1.0, -1.0, False),
    (-1.0, -1.0 - 1.0e-4, True),
]


@pytest.mark.parametrize("less_than_target, x, expect", less_than_params)
def test_less_than(less_than_target, x, expect):
    assert less_than(less_than_target)(x) is expect


is_negative_params = [
    (-1e-4, True),
    (0, False),
    (1e-4, False),
]


@pytest.mark.parametrize("n, expect", is_negative_params)
def test_is_negative(n, expect):
    assert is_negative(n) is expect


more_than_or_eq_params = [
    (1.0, 1.0 + 1.0e-4, True),
    (1.0, 1.0, True),
    (1.0, 1.0 - 1.0e-4, False),
    (-1.0, -1.0 + 1.0e-4, True),
    (-1.0, -1.0, True),
    (-1.0, -1.0 - 1.0e-4, False),
]


@pytest.mark.parametrize("more_than_target, x, expect", more_than_or_eq_params)
def test_more_than_or_eq(more_than_target, x, expect):
    assert more_than_or_eq(more_than_target)(x) is expect


less_than_or_eq_params = [
    (1.0, 1.0 - 1.0e-4, True),
    (1.0, 1.0, True),
    (1.0, 1.0 + 1.0e-4, False),
    (-1.0, -1.0 + 1.0e-4, False),
    (-1.0, -1.0, True),
    (-1.0, -1.0 - 1.0e-4, True),
]


@pytest.mark.parametrize("less_than_target, x, expect", less_than_or_eq_params)
def test_less_than_or_eq(less_than_target, x, expect):
    assert less_than_or_eq(less_than_target)(x) is expect


is_sub_params: Tuple = (
    ({}, True),
    ({1, 2}, True),
    ({1, 2, 3}, True),
    ({1, 2, 3, 4}, False),
)


@pytest.mark.parametrize("target, expect", is_sub_params)
def test_is_sub(target, expect):
    c = TestCollection({1, 2, 3})
    assert is_sub(c)(target) is expect


is_type_params: Tuple = (
    (3, int, True),
    (2.0, int, False),
    (1, bool, False),
    (True, int, False),
)


@pytest.mark.parametrize("obj, type_, expect", is_type_params)
def test_is_type(obj, type_, expect):
    assert is_type(type_)(obj) is expect


def test_is_type_str_case():

    class A:
        ...

    assert is_type("A")(A()) is True
    assert is_type("B")(A()) is False


is_instance_params: Tuple = (
    (3, int, True),
    (2.0, int, False),
    (1, bool, False),
    (True, int, True),
)


@pytest.mark.parametrize("obj, type_, expect", is_instance_params)
def test_is_instance(obj, type_, expect):
    assert is_instance(type_)(obj) is expect


def test_is_instance_str_case():

    class A:
        ...

    class B(A):
        ...

    assert is_instance("A")(B())
    assert not is_instance("int")(B())


is_in_params: Tuple = (
    ([1, 2, 3], 0, False),
    ([1, 2, 3], 2, True),
    ([1, None, 3], None, True),
    ([1, 2, 3], None, False),
)


@pytest.mark.parametrize("c, target, expect", is_in_params)
def test_is_in(c, target, expect):
    assert is_in(c)(target) is expect


in_range_params = (
    (0, True),
    (1, True),
    (2, True),
    (3, False),
)


@pytest.mark.parametrize("until", (3,))
@pytest.mark.parametrize("target, expect", in_range_params)
def test_in_range(until, target, expect):
    cond_f = in_range(until)
    assert cond_f(target) is expect


in_range_params_two_args = (
    (0, False),
    (1, True),
    (2, True),
    (3, False),
)


@pytest.mark.parametrize("start, until", ((1, 3),))
@pytest.mark.parametrize("target, expect", in_range_params_two_args)
def test_in_range_two_args(start, until, target, expect):
    cond_f = in_range(start, until)
    assert cond_f(target) is expect


not_found_path = Path(Path(__file__).parent / "not_found")
not_found_filename = str(not_found_path)
exists_path_params = [
    (__file__, True),
    (Path(__file__), True),
    (not_found_path, False),
    (not_found_filename, False),
]


@pytest.mark.parametrize("path, expect", exists_path_params)
def test_exists_path(path, expect):
    assert exists_path(path) is expect
