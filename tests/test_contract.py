#!/usr/bin/env python
# -*- coding: utf-8 -*-

from inspect import signature
from io import StringIO

from pytest import raises

from ycontract import ContractState, InContractException, OutContractException
from ycontract._contract import contract, pops_out_cond_f


def test_pops_out_condf():
    cond_kw = {'a': lambda a: a < 0, 'res': lambda x: x < 10}

    def f(a):
        return a * a

    conds = pops_out_cond_f(cond_kw, f)  # conds == [lambda x: x < 10]
    assert list(cond_kw.keys()) == ['a']
    assert len(conds) == 1
    cond_f = conds[0]
    assert cond_f(9)
    assert not cond_f(10)


def test_pops_out_cond_f_in_case_of_several_returns_contracts():
    cond_kw = {'a': lambda a: a < 0, 'res': [lambda x: x < 10, lambda x: x > 4]}

    def f(a):
        return a * a

    # conds == [lambda x: x < 10, lambda x: x > 4, lambda x: x > 4]
    conds = pops_out_cond_f(cond_kw, f)
    assert list(cond_kw.keys()) == ['a']
    assert len(conds) == 2

    cond_f = conds[0]
    assert cond_f(9)
    assert not cond_f(10)

    cond_f = conds[1]
    assert not cond_f(4)
    assert cond_f(5)


def test_pops_out_cond_f_in_case_of_some_return_variable_is_used():
    cond_kw = {'res': lambda x: x < 0, 'result': lambda x: x < 0, 'returns': lambda x: x > 0}

    def f(res, result):
        return res - result

    # conds == lambda x: x > 0
    conds = pops_out_cond_f(cond_kw, f)
    assert list(cond_kw.keys()) == ['res', 'result']
    assert len(conds) == 1

    cond_f = conds[0]
    assert not cond_f(0)
    assert cond_f(1)


def test_contract_fix_signature():

    def add(a, b, c, d=2, e=3):
        return a + b + c + d + e

    add2 = contract()(add)
    assert signature(add) == signature(add2)


def test_contract_fix_signature_case_disable(disable_contract_state: ContractState):

    def add(a, b, c, d=2, e=3):
        return a + b + c + d + e

    add2 = contract(contract_state=disable_contract_state)(add)
    assert signature(add) == signature(add2)


def test_contract_only_once_output():

    io = StringIO()

    @contract()
    def show(s: str):
        print(s, file=io)

    show("Hello")
    text = io.getvalue()
    assert text.count("Hello") == 1


def test_contract_in_contract():

    @contract(lambda a: a > 0)
    def sub(a, b):
        return a - b

    with raises(InContractException):
        sub(-1, 2)

    sub(1, 2)


def test_contract_for_disable_state(disable_contract_state: ContractState):

    @contract(lambda a: a > 0, returns=lambda res: res > 0, contract_state=disable_contract_state)
    def sub(a, b):
        return a - b

    sub(-1, 2)


def test_contract_for_disable2():
    contract_state = ContractState()

    @contract(lambda a: a > 0, contract_state=contract_state)
    def f(a):
        return a

    contract_state.disable()

    f(-1)


def test_contract_for_complex_case():

    @contract(
        lambda a: a > 0,
        lambda b: b > 0,
        lambda a, b: a > 1 or b > 1,
        returns=lambda res: res > 0,
    )
    def sub(a, b):
        return a - b

    with raises(InContractException):
        sub(-1, 2)

    with raises(OutContractException):
        sub(1, 2)

    sub(3, 2)


def test_contract_deepcopy_conds_and_cond_opts():
    conds = [lambda a: a > 0]
    cond_opts = {"b": lambda b: b > 0}

    @contract(*conds, **cond_opts)
    def add(a, b):
        return a + b

    add(1, 1)

    with raises(InContractException):
        add(-1, 1)

    with raises(InContractException):
        add(1, -1)

    assert len(conds) == 1
    assert set(cond_opts.keys()) == {"b"}
