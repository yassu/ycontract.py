#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ycontract.ycontract import ContractState, disable_contract


def test_ContractState_enable():
    state = ContractState()
    state.disable()
    state.enable()
    assert state.is_enable


def test_ContractState_disable():
    state = ContractState()
    state.disable()
    assert not state.is_enable


def test_ContractState_is_enable():
    state = ContractState()
    state.disable()
    assert not state.is_enable
    state.enable()
    assert state.is_enable


def test_ContractState_is_disable():
    state = ContractState()
    state.disable()
    assert state.is_disable
    state.enable()
    assert not state.is_disable


def test_ContractState_str_is_enable():
    state = ContractState(True)
    assert str(state) == "ContractState<is_enable=True>"


def test_ContractState_str_is_disable():
    state = ContractState(False)
    assert str(state) == "ContractState<is_enable=False>"


def test_ContractState_repr_is_enable():
    state = ContractState(True)
    assert repr(state) == "ContractState<is_enable=True>"


def test_ContractState_repr_is_disable():
    state = ContractState(False)
    assert repr(state) == "ContractState<is_enable=False>"


def test_disable_contract():
    state = ContractState()
    disable_contract(state=state)
    assert not state.is_enable
